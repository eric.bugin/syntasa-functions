class pyspark_dataframe_editor:
    
    def __init__(self, df):
        self.df = df

    def match_output_table_schema(self, output_table):
        
        output_table_schema = spark.sql(f'DESCRIBE {output_table}').toPandas().iloc[:-3]
        output_table_headers = output_table_schema['col_name'].tolist()
        output_table_dtypes = output_table_schema['data_type'].tolist()
        output_table_schema_zip = dict(zip(output_table_schema['col_name'], output_table_schema['data_type']))
        df_columns_list = self.df.columns

        # add missing columns that were found in the output table and not in the dataframe
        for header,dtype in output_table_schema_zip.items():
            missing_column_to_add = [x for x in [header] if x not in df_columns_list]
            if len(missing_column_to_add) > 0:
                print(f'Adding missing column: {missing_column_to_add[0]}')
                self.df = self.df.withColumn(header, F.lit(None).cast(dtype))

        # delete excess columns that were found in the dataframe and not in the output table
        excess_columns_to_delete = [x for x in df_columns_list if x not in output_table_headers]
        for header in excess_columns_to_delete:
            print(f'Deleting excess column: {header}')
            self.df = self.df.drop(header)
        
        # sort headers in the same order as the output table
        self.df = self.df.select(output_table_headers)
        
        return self.df
