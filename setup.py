from setuptools import setup

setup(
    name='syntasa-functions',
    version='0.1.0',
    description='A Python module for editing PySpark DataFrames',
    url='https://gitlab.com/eric.bugin/syntasa-functions',
    packages=['syntasa_functions'],  # Replace with the actual package name or module name
    py_modules=['pyspark_dataframe_editor'],
    install_requires=[
        'pyspark',  # Add any other dependencies your module requires
    ],
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
)
